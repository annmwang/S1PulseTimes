#ifndef S1PulseTimes_H
#define S1PulseTimes_H

#include "Analysis.h"

#include "CutsS1PulseTimes.h"
#include "EventBase.h"

#include <TTreeReader.h>
#include <string>

class SkimSvc;

class S1PulseTimes : public Analysis {

public:
    S1PulseTimes();
    ~S1PulseTimes();

    void Initialize();
    void Execute();
    void Finalize();

    // Output TTree variables
    double evt_triggerTS;
    int runID;
    int eventID;
    int triggerType;
    vector<float> S1pulseAreas;
    vector<float> S1maxCHFracs;
    vector<float> S1maxCHs;
    vector<bool>  S1isHSC;

protected:
    CutsS1PulseTimes* m_cutsS1PulseTimes;
    ConfigSvc* m_conf;
};

#endif
