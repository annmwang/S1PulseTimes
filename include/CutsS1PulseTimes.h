#ifndef CutsS1PulseTimes_H
#define CutsS1PulseTimes_H

#include "EventBase.h"

class CutsS1PulseTimes {

public:
    CutsS1PulseTimes(EventBase* eventBase);
    ~CutsS1PulseTimes();
    bool S1PulseTimesCutsOK();

private:
    EventBase* m_event;
};

#endif
