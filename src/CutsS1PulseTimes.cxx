#include "CutsS1PulseTimes.h"
#include "ConfigSvc.h"

CutsS1PulseTimes::CutsS1PulseTimes(EventBase* eventBase)
{
    m_event = eventBase;
}

CutsS1PulseTimes::~CutsS1PulseTimes()
{
}

// Function that lists all of the common cuts for this Analysis
bool CutsS1PulseTimes::S1PulseTimesCutsOK()
{
    // List of common cuts for this analysis into one cut
    return true;
}
