#include "S1PulseTimes.h"
#include "Analysis.h"
#include "ConfigSvc.h"
#include "CutsBase.h"
#include "CutsS1PulseTimes.h"
#include "EventBase.h"
#include "HistSvc.h"
#include "Logger.h"
#include "SkimSvc.h"
#include "SparseSvc.h"
#include <tuple>

// Constructor
S1PulseTimes::S1PulseTimes()
    : Analysis()
{
    // Set up the expected event structure, include branches required for analysis.
    // List the branches required below, see full list here https://luxzeplin.gitlab.io/docs/softwaredocs/analysis/analysiswithrqs/rqlist.html
    // Load in the Single Scatters Branch
    m_event->IncludeBranch("pulsesTPC");
    m_event->IncludeBranch("pulsesTPCHG");
    m_event->IncludeBranch("pulsesSkin");
    m_event->IncludeBranch("pulsesODHG");
    m_event->Initialize();
    ////////

    // Setup logging
    logging::set_program_name("S1PulseTimes Analysis");

    // Setup the analysis specific cuts.
    m_cutsS1PulseTimes = new CutsS1PulseTimes(m_event);

    // Setup config service so it can be used to get config variables
    m_conf = ConfigSvc::Instance();
}

// Destructor
S1PulseTimes::~S1PulseTimes()
{
    delete m_cutsS1PulseTimes;
}

/////////////////////////////////////////////////////////////////
// Start of main analysis code block
//
// Initialize() -  Called once before the event loop.
void S1PulseTimes::Initialize()
{
    INFO("Initializing S1PulseTimes Analysis");
    m_cuts->sr1()->Initialize();
    auto tree = std::make_tuple(
                                std::make_pair("evt_triggerTS", &evt_triggerTS),
                                std::make_pair("runID", &runID),
                                std::make_pair("eventID", &eventID),
                                std::make_pair("triggerType", &triggerType),
                                std::make_pair("S1pulseAreas", &S1pulseAreas),
                                std::make_pair("S1maxCHFracs", &S1maxCHFracs),
                                std::make_pair("S1maxCHs", &S1maxCHs),
                                std::make_pair("S1isHSC", &S1isHSC)
                                );
    m_hists->BookTree("S1_pulses", tree);
}

// Execute() - Called once per event.
void S1PulseTimes::Execute()
{
  m_cuts->sr1()->InitializeETrainVetoInEvent();

  evt_triggerTS  = (*m_event->m_eventHeader)->triggerTimeStamp_s + (*m_event->m_eventHeader)->triggerTimeStamp_ns*1.E-9;
  runID              = (*m_event->m_eventHeader)->runID;
  eventID            = (*m_event->m_eventHeader)->eventID;
  triggerType        = (*m_event->m_eventHeader)->triggerType;
  
  int nS1s   = (*m_event->m_tpcPulses)->s1PulseIDs.size();
  bool isMuon = m_cuts->sr1()->IsTPCMuon();

  // Loop through all S1 pulses
  S1pulseAreas.clear();
  S1maxCHFracs.clear();
  S1maxCHs.clear();
  S1isHSC.clear();
  
  // Apply muon veto
  if (m_cuts->sr1()->TPCMuonVeto()) {
    
    for (unsigned int iS1 = 0; iS1 < nS1s; iS1++){
      int pulseID      = (*m_event->m_tpcPulses)->s1PulseIDs[iS1];
      int nCoinc       = (*m_event->m_tpcPulses)->coincidence[pulseID];
      float pulseArea  = (*m_event->m_tpcPulses)->pulseArea_phd[pulseID];
      // int nChannels            = (*m_event->m_tpcPulses)->chID[pulseID].size();
      // int nChannelsWithPhotons = (*m_event->m_tpcPulses)->chPhotonCount[pulseID].size();
      // int start_time = (*m_event->m_tpcPulses)->pulseStartTime_ns[pulseID];
      // int peak_time  = (*m_event->m_tpcPulses)->peakTime_ns[pulseID];
      // int end_time   = (*m_event->m_tpcPulses)->pulseEndTime_ns[pulseID];
      // int fall_time  = end_time - start_time - peak_time;
      // float TBA = (*m_event->m_tpcPulses)->topBottomAsymmetry[pulseID];
      
      // Apply e-train veto
      if (!m_cuts->sr1()->PassesETrainVeto(pulseID, "S1"))
        continue;
      
      std::vector<float> ch_pulseAreas = (*m_event->m_tpcPulses)->chPulseArea_phd[pulseID];
      int imax = std::distance(ch_pulseAreas.begin(),std::max_element(ch_pulseAreas.begin(), ch_pulseAreas.end()));
      float fracHighestCh = ch_pulseAreas[imax]/pulseArea;
      int maxCH = (*m_event->m_tpcPulses)->chID[pulseID][imax];
      bool isHSC = (ch_pulseAreas[imax] > (pow(pulseArea,0.4) + 1));
      
      S1pulseAreas.push_back(pulseArea);
      S1maxCHs.push_back(maxCH);
      S1maxCHFracs.push_back(fracHighestCh);
      S1isHSC.push_back(isHSC);
      
    }
    m_hists->FillTree("S1_pulses");
  }
  
  m_cuts->sr1()->DeleteInactiveProgenitors();
}

// Finalize() - Called once after event loop.
void S1PulseTimes::Finalize()
{
    INFO("Finalizing S1PulseTimes Analysis");
}
